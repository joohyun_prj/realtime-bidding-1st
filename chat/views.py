from __future__ import print_function
from django.shortcuts import render
from django.contrib.auth import authenticate, logout, login
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from bidding import settings

from realtime_bidding.models import Chat, Chatting_room

def Login(request, room_number=None):
    if room_number is None:
        next = request.GET.get('next', '/chat/new_chat_room/')
    else:
        next = request.GET.get('next', '/chat/home/' + str(room_number))

    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(next)
            else:
                return HttpResponse("Account is not active at the moment.")
        else:
            return HttpResponseRedirect(settings.LOGIN_URL)
    return render(request, "chat/login.html", {'next': next})

def Logout(request):
    logout(request)
    return HttpResponseRedirect('/chat/login/')

def NewChatRoom(request):
    try:
        latest_room = Chatting_room.objects.latest('room_id')
        latest_room_id = latest_room.room_id
    except:
        latest_room_id = -1
    new_room_id = latest_room_id +1
    print(str(new_room_id) +"_"+str(type(new_room_id)))

    new_chat_room = Chatting_room.objects.create(room_id=new_room_id)
    c = new_chat_room.chat_set.all()

    return render(request, "chat/home.html", {'home': 'active', 'chat': c, "room_number":new_room_id})

def Home(request,room_number):
    chat_room = Chatting_room.objects.get(room_id=int(room_number))
    c = chat_room.chat_set.all()

    print(c)

    return render(request, "chat/home.html", {'home': 'active', 'chat': c, "room_number":room_number})


def Post(request,room_number):
    if request.method == "POST":
        msg = request.POST.get('msgbox', None)
        chat_room = Chatting_room.objects.get(room_id=int(room_number))
        print(chat_room)
        if msg != '':
            c = chat_room.chat_set.create(user=request.user, message=msg)
            # c.save()
        return JsonResponse({ 'msg': msg, 'user': c.user.username })
    else:
        return HttpResponse('Request must be POST.')

def Messages(request,room_number):
    chat_room = Chatting_room.objects.get(room_id=int(room_number))
    print(chat_room)
    c = chat_room.chat_set.all()
    print(c)
    return render(request, 'chat/messages.html', {'chat': c})
