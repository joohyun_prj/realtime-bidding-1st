from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login/$', views.Login, name='login_new_chatroom'),
    url(r'^login/(?P<room_number>\d+)/$', views.Login, name='login'),
    url(r'^logout/$', views.Logout, name='logout'),

    url(r'^home/(?P<room_number>\d+)/$', views.Home, name='home'),
    url(r'^new_chat_room/$', views.NewChatRoom, name='new_chat_room'),


    url(r'^post/(?P<room_number>\d+)/$', views.Post, name='post'),
    url(r'^messages/(?P<room_number>\d+)/$', views.Messages, name='messages'),
]