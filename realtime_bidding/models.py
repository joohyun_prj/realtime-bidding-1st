from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User as auth_User
# Create your models here.

class User(models.Model):
    naver_id=models.CharField(max_length=20,primary_key=True)
    email=models.EmailField()

    def __unicode__(self):
        return self.naver_id

class Product(models.Model):
    product_id=models.AutoField(primary_key=True)
    chatting_room_id=models.ManyToManyField('Chatting_room',related_name='chatting_room_id')
    product_name=models.CharField(max_length=50)
    start_price=models.PositiveIntegerField(default=0)
    bidding_price=models.PositiveIntegerField(null=True,blank=True)
    seller_id=models.ManyToManyField('User',related_name='seller_id')
    buyer_id=models.ManyToManyField('User',related_name='buyer_id',blank=True)

    def __unicode__(self):
        return self.product_name

class Chatting_room(models.Model):
    room_id=models.AutoField(primary_key=True)
    #product_id=models.ForeignKey('Product')
    chatting_history_file_path=models.CharField(max_length=300)
    bidding_history_file_path=models.CharField(max_length=300)
    #chatting_history_file_path=models.FilePathField(path=settings.FILE_PATH_FIELD_DIRECTORY)
    #bidding_history_file_path=models.FilePathField(path=settings.FILE_PATH_FIELD_DIRECTORY)

    def __unicode__(self):
        return '%s' % (self.room_id)

class Chat(models.Model):
    chat_room = models.ForeignKey(Chatting_room, on_delete=models.CASCADE, default=9999)

    created = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(auth_User)
    message = models.CharField(max_length=200)

    def __unicode__(self):
        return self.message