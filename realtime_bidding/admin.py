from django.contrib import admin
from realtime_bidding.models import User,Product,Chatting_room

# Register your models here.

admin.site.register(User)
admin.site.register(Product)
admin.site.register(Chatting_room)
