from django.shortcuts import render
from django.template import loader, Context
from django.views.generic import ListView

from .models import Chatting_room

# Create your views here.
MAX_SIZE_OF_CHATROOM_IN_A_PAGE = 8
class main(ListView):
    #context = Context() # initialize for html parameter

    template_name = 'index.html'
    page_num = 0
    queryset = Chatting_room.objects.all().order_by('room_id')
    paginate_by = 8
    #chat_room = Chatting_room.objects.all().order_by('room_id')[:(page_num+1)*MAX_SIZE_OF_CHATROOM_IN_A_PAGE] # retrieve chat_room
    #context['show_chat'] = chat_room
    #context['loop_times'] = range(0, 3)
    #context['account'] = 0
    #return render(request, 'index.html', context)